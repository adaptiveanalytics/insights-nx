// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import PKG from '../../../../package.json';

export const environment = {
  production: false,
  appTitle: 'Insights by Xplor',
  appName: PKG.name,
  appVersion: PKG.version,
  apiUrl: 'http://localhost:3000/',

  datadog: {
    clientToken: 'pub7d4fcc4b474cf29e9c88620d4257b86e',
    site: 'us5.datadoghq.com',
    env: 'local',
  },

  firebaseConfig: {
    apiKey: 'AIzaSyAaFMZbJhcecP5tr3RsC7xVk6Lwo2X4mTM',
    authDomain: 'xplor-insights.firebaseapp.com',
    projectId: 'xplor-insights',
    storageBucket: 'xplor-insights.appspot.com',
    messagingSenderId: '243050821603',
    appId: '1:243050821603:web:3591d9faa8d460440952d6',
    measurementId: 'G-YYGW7FRJ9V',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
