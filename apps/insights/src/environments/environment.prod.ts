import PKG from '../../../../package.json';

export const environment = {
  production: true,
  appTitle: 'Insights by Xplor',
  appName: PKG.name,
  appVersion: PKG.version,
  apiUrl: 'http://localhost:3000/',

  datadog: {
    clientToken: 'pub7d4fcc4b474cf29e9c88620d4257b86e',
    site: 'us5.datadoghq.com',
    env: 'prod',
  },

  firebaseConfig: {
    apiKey: 'AIzaSyAaFMZbJhcecP5tr3RsC7xVk6Lwo2X4mTM',
    authDomain: 'xplor-insights.firebaseapp.com',
    projectId: 'xplor-insights',
    storageBucket: 'xplor-insights.appspot.com',
    messagingSenderId: '243050821603',
    appId: '1:243050821603:web:3591d9faa8d460440952d6',
    measurementId: 'G-YYGW7FRJ9V',
  },
};
