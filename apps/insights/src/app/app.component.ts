import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from '../environments/environment';

import { LoggingService, PageService, StoreService } from '@insights-xplor/shared';
import { UserService } from '@insights-xplor/user';
import { AuthService } from '@insights-xplor/auth';
import { NoticesService } from '@insights-xplor/shared';

@Component({
  selector: 'insights-xplor-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'insights-mono';
  user = false;

  private userState$?: Subscription;

  constructor(
    private authService: AuthService,
    private loggingService: LoggingService,
    private noticesService: NoticesService,
    private pageService: PageService,
    private router: Router,
    private storeService: StoreService,
    private userService: UserService,
  ) {
    pageService.baseTitle = environment.appTitle;
  }

  ngOnInit(): void {
    this.pageService.setTitle('home');

    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        this.log(ev, 'NavigationEnd', 'debug');
      }
    });

    this.userState$ = this.userService.insightsUserState$.subscribe((user) => {
      this.user = !!user;

      if (user) {
        this.log(user, 'userState$', 'info');
      } else {
        this.log('no user', 'userState$', 'info');
        this.router.navigate(['/auth/login']).catch(e => this.log(e, 'userState$', 'error'));
        return;
      }
    });

    this.log(this, null, 'debug');
  }

  ngOnDestroy(): void {
    this.userState$?.unsubscribe();
  }

  clearUser(ev: unknown): void {
    this.authService.logout(ev);
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
