import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { AuthGuard } from '@insights-xplor/auth';

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('@insights-xplor/dashboard').then(m => m.DashboardModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'user',
    loadChildren: () => import('@insights-xplor/user').then(m => m.UserModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'home',
    component: LandingPageComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
