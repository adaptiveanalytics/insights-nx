import { Component, OnDestroy, OnInit } from '@angular/core';

import { LoggingService, PageService } from '@insights-xplor/shared';
import { UserService } from '@insights-xplor/user';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'insights-xplor-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.less'],
})
export class LandingPageComponent implements OnInit, OnDestroy {

  private userSub$?: Subscription;

  constructor(
    private loggingService: LoggingService,
    private pageService: PageService,
    private router: Router,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.pageService.setTitle('home');

    this.userSub$ = this.userService.insightsUserState$.subscribe((user) => {
      if (user) {
        const { landing_page } = user.details;

        if (landing_page) {
          this.router.navigate([landing_page]).catch(e => this.log(e, 'userSub$', 'error'));
        }

      }
    });

    this.log(this, null);
  }

  ngOnDestroy(): void {
    this.userSub$?.unsubscribe();
  }

  log(msg: unknown, label?: string | string[] | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
