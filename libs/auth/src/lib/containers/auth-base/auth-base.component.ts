import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../auth.service';
import { LoggingService } from '@insights-xplor/shared';
import { Router } from '@angular/router';

@Component({
  selector: 'insights-xplor-auth-base',
  templateUrl: './auth-base.component.html',
  styleUrls: ['./auth-base.component.less'],
})
export class AuthBaseComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private loggingService: LoggingService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    if (this.authService.authUser$) {
      this.router.navigate(['/home']).catch(e => this.log(e, 'authUser$', 'error'));
    }

    this.log(this, null, 'debug');
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
