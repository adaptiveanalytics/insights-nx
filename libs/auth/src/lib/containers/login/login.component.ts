import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { LoggingService, PageService } from '@insights-xplor/shared';
import { Authenticate } from '@insights-xplor/data-models';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'insights-xplor-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private loggingService: LoggingService,
    private pageService: PageService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.pageService.setTitle('login');

    this.log(this, null, 'debug');
  }

  login(authenticate: Authenticate): void {
    this.authService
      .login(authenticate)
      .subscribe((user) => {
        if (user) {
          this.router.navigate(['/home']).catch(e => this.log(e, 'login', 'error'));
        }
      });
  }

  log(msg: unknown, label?: string | string[] | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
