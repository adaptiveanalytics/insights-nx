import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { Authenticate } from '@insights-xplor/data-models';
import { LoggingService } from '@insights-xplor/shared';

@Component({
  selector: 'insights-xplor-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.less'],
})
export class LoginFormComponent implements OnInit {
  @Output() formSubmit = new EventEmitter<Authenticate>();
  usernameForm = 'insights-demo@xplortechnologies.com';
  passwordForm = 'Qwerty123';

  constructor(
    private loggingService: LoggingService,
  ) {
  }

  ngOnInit(): void {
    this.log(this, null, 'debug');
  }

  login(): void {
    const authenticate: Authenticate = { username: this.usernameForm.valueOf(), password: this.passwordForm.valueOf() };
    this.formSubmit.emit(authenticate);
  }

  log(msg: string | object, label?: string | string[] | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
