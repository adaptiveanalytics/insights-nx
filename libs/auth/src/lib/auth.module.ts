import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './containers/login/login.component';
import { SharedModule } from '@insights-xplor/shared';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { AuthService } from './auth.service';
import { ModuleConfig } from '@insights-xplor/data-models';
import { AuthBaseComponent } from './containers/auth-base/auth-base.component';

export const authRoutes: Routes = [
  {
    path: 'auth',
    component: AuthBaseComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
    ]
  },
];
const COMPONENTS: unknown[] = [
  LoginComponent,
  LoginFormComponent,
  AuthBaseComponent,
];

@NgModule({
  declarations: [
    COMPONENTS,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(authRoutes),
  ],
  exports: [
    [COMPONENTS],
    LoginComponent,
  ],
  providers: [
    AuthService,
  ],
})
export class AuthModule {
  static forRoot(config: ModuleConfig): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [{ provide: 'config', useValue: config }],
    };
  }
}
