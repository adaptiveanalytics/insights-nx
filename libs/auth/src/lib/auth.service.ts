import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { LoggingService, StoreService } from '@insights-xplor/shared';
import { UserService } from '@insights-xplor/user';
import { Authenticate, InsightsUser, ModuleConfig } from '@insights-xplor/data-models';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private env$: ModuleConfig;

  public authUser$?: InsightsUser;
  private authSubject$ = new BehaviorSubject<InsightsUser | null>(null);
  authState$: Observable<InsightsUser | null> = this.authSubject$.asObservable();

  constructor(
    @Inject('config') private config: ModuleConfig,
    private httpService: HttpClient,
    private loggingService: LoggingService,
    private storeService: StoreService,
    private userService: UserService,
  ) {
    this.env$ = config;
    const user = this.storeService.retrieve('user') as InsightsUser;

    if (user) {
      this.authSubject$.next(user);
      this.userService.loadUser();
    }

    this.authState$.subscribe((user) => {
      if (user) {
        this.authUser$ = user;
      }
    });

    this.log(this, null, 'debug');
  }

  login(authenticate: Authenticate): Observable<InsightsUser | void> {
    this.log(authenticate, 'login', 'debug');

    return of(this.userService.insightsUser$).pipe(
      tap((user: InsightsUser) => {
        this.authSubject$.next(user);
        this.userService.loadUser();
      }));
  }

  logout(ev: unknown): void {
    this.log(ev, 'logout', 'info');
    this.authSubject$.next(null);
    this.userService.clearUser();
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
