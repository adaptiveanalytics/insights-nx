import { LocationEntity, TenantEntity } from './entity';

export type InsightUsersPermission = 'can_intercom_chat'
  | 'can_view_intellihub_corporate_reporting'
  | 'can_view_intellihub_studio_reporting'
  | 'can_view_intellihub_admin'
  | 'can_view_intellihub_super_admin'
  | 'ih_admin'
  | 'all';

export interface InsightsUser {
  details: InsightsUserDetails;
  permissions: InsightUsersPermission[];
  locations: LocationEntity[];
  locationIds?: LocationEntity['id'][];
  tenants: TenantEntity[];
}

export interface InsightsUserDetails {
  userId: string;
  first_name: string;
  last_name: string;
  full_name: string;
  initials: string;
  email: string;
  birth_date: string | null;
  company_name: string;
  role_lvl?: number;
  brandId?: number;
  admin?: boolean;
  subdomain?: string;
  landing_page?: string;
}
