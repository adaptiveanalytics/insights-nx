export type WidgetTypes = 'xy-chart' | string;

export interface Widget {
  id: string;
  chart: WidgetTypes;
}
