import { Widget } from './widgets';

export interface Dashboard {
  config?: DashboardConfig;
  id: string;
  isCustom?: boolean;
  title: string;
  tiles: DashboardTile[];
}

export interface DashboardConfig {
  rows: number;
}

export interface DashboardTile {
  id: string;
  title: string;
  widget_id: Widget['id'];
}
