export interface Entity {
  id: string;
  name?: string;
}

export interface TenantEntity extends Entity {
  name: string;
  brandId: number;
  schemaName: string;
}

export interface LocationEntity extends Entity {
  name: string;
}
