export interface EnvConfig {
  production: boolean;
  appName?: string;
  appTitle?: string;
  appVersion?: string;
}
