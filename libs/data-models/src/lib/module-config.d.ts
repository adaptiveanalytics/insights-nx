export interface ModuleConfig {
  production: boolean;

  apiUrl: string;
  appTitle: string;
  appName: string;
  appVersion: string;

  datadog: {
    clientToken: string;
    site: string;
    service?: string;
    env?: string;
    version?: string;
    forwardErrorsToLogs?: boolean;
    sampleRate?: number;
    silentMultipleInit?: boolean;
    proxyUrl?: boolean;
  };

  firebaseConfig: {
    apiKey: string;
    authDomain: string;
    projectId: string;
    storageBucket: string;
    messagingSenderId: string;
    appId: string;
    measurementId: string;
  };
}
