import { MatSnackBarConfig } from '@angular/material/snack-bar';

export interface ErrorMessage extends Error {
  name?: string;
  message: string;
  errorCode?: string;
}

export type NoticeTypes = 'notice' | 'success' | 'warning' | 'error';

export interface NoticeConfig extends MatSnackBarConfig {
  sound?: string;
  noticeType: NoticeTypes;
}

export interface NoticeObject {
  message: string;
  action?: string;
  noticeType?: NoticeTypes;
}
