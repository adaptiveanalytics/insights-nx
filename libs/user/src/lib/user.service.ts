import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { AngularFirestore } from '@angular/fire/compat/firestore';

import { LoggingService, StoreService } from '@insights-xplor/shared';
import { InsightsUser, InsightUsersPermission, ModuleConfig } from '@insights-xplor/data-models';
import demoUser from '../assets/demo-user.json';

@Injectable({ providedIn: 'root' })
export class UserService {

  private readonly adminPermissions$: InsightUsersPermission[] = ['can_view_intellihub_admin', 'can_view_intellihub_super_admin'];
  private env$: ModuleConfig;
  insightsUser$: InsightsUser = demoUser as InsightsUser;

  private insightsUserSubject$ = new BehaviorSubject<InsightsUser | null>(null);
  insightsUserState$: Observable<InsightsUser | null> = this.insightsUserSubject$.asObservable();

  constructor(
    @Inject('config') private config: ModuleConfig,
    private afs: AngularFirestore,
    private loggingService: LoggingService,
    private router: Router,
    private storeService: StoreService,
  ) {
    this.env$ = this.config;

    this.log(this, null, 'debug');
  }

  clearUser(ev?: unknown): void {
    if (ev) {
      this.log(ev, 'clearUser', 'debug');
    }

    this.insightsUserSubject$.next(null);
    this.storeService.clear();
  }

  loadUser(): void {
    const user = this.normalizeUser(this.insightsUser$);

    this.storeService.save('user', user);
    this.insightsUserSubject$.next(user);
  }

  normalizeUser(user: InsightsUser): InsightsUser {
    user.details.admin = _.intersection(this.adminPermissions$, user.permissions).length > 0;
    user.details.full_name = [user.details.first_name, user.details.last_name].join(' ');
    user.details.initials = [user.details.first_name.slice(0, 1), user.details.last_name.slice(0, 1)].join('');
    user.locationIds = user.locations?.map(l => l.id);

    return user;
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
