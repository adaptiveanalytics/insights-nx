import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { InsightsUser } from '@insights-xplor/data-models';
import { LoggingService, PageService } from '@insights-xplor/shared';
import { UserService } from '../user.service';

@Component({
  selector: 'insights-xplor-user-base',
  templateUrl: './user-base.component.html',
  styleUrls: ['./user-base.component.less'],
})
export class UserBaseComponent implements OnInit, OnDestroy {
  insightsUser?: InsightsUser;

  private userState$?: Subscription;

  constructor(
    private loggingService: LoggingService,
    private pageService: PageService,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.pageService.setTitle('user');

    this.userState$ = this.userService.insightsUserState$.subscribe((user) => {
      if (user) {
        this.insightsUser = user;
      }
    });

    this.log(this, null, 'debug');
  }

  ngOnDestroy(): void {
    this.userState$?.unsubscribe();
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
