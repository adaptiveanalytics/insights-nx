import { ModuleWithProviders, NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { SharedModule } from '@insights-xplor/shared';
import { ModuleConfig } from '@insights-xplor/data-models';
import { UserBaseComponent } from './user-base/user-base.component';


export const userRoutes: Route[] = [
  {
    path: '',
    component: UserBaseComponent,
  },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(userRoutes),
  ],
  declarations: [
    UserBaseComponent,
  ],
})
export class UserModule {
  static forRoot(config: ModuleConfig): ModuleWithProviders<UserModule> {
    return {
      ngModule: UserModule,
      providers: [{ provide: 'config', useValue: config }],
    };
  }
}
