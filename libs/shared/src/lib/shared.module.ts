import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { HttpClientModule } from '@angular/common/http';
import PKG from '../../../../package.json';

import { MaterialModule } from './modules/material.module';
import { LoadingScreenComponent } from './components/loading-screen/loading-screen.component';
import { LoadingPanelComponent } from './components/loading-panel/loading-panel.component';
import { LoggingService } from './services/logging.service';
import { PageService } from './services/page.service';
import { StoreService } from './services/store.service';
import { ErrorHandlerService } from './services/error-handler.service';
import { BaseComponent } from './components/base/base.component';
import { NavComponent } from './components/nav/nav.component';
import { ModuleConfig } from '@insights-xplor/data-models';
import { httpInterceptorProviders } from './http-interceptors';
import { NoticesService } from './services/notices.service';
import { NoticeViewerComponent } from './components/notice-viewer/notice-viewer.component';

const COMPONENTS: unknown[] = [
  LoadingScreenComponent,
  LoadingPanelComponent,
  BaseComponent,
  NavComponent,
  NoticeViewerComponent,
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    LayoutModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgxWebstorageModule.forRoot({ prefix: PKG.name }),
    HttpClientModule,
  ],
  exports: [
    [COMPONENTS],
    CommonModule,
    LayoutModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    Title,
    PageService,
    LoggingService,
    ErrorHandlerService,
    StoreService,
    httpInterceptorProviders,
    NoticesService,
  ],
})
export class SharedModule {
  static forRoot(config: ModuleConfig): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [{ provide: 'config', useValue: config }],
    };
  }
}
