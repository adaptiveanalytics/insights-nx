import { Inject, Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ModuleConfig } from '@insights-xplor/data-models';

@Injectable({ providedIn: 'root' })
export class PageService {
  private env$: ModuleConfig;
  private appTitle$: string = this.baseTitle;
  private baseTitle$: string | undefined;

  constructor(
    @Inject('config') private config: ModuleConfig,
    private titleSvc: Title,
  ) {
    this.env$ = config;
    this.baseTitle$ = this.config.appTitle;
  }

  setTitle(title: string): void {
    if (!title.includes(this.baseTitle)) {
      title = [title, this.baseTitle].join(' | ');
    }

    title = title.toLowerCase();
    this.titleSvc.setTitle(title);
    this.appTitle$ = title;
  }

  get appTitle(): string {
    return this.appTitle$;
  }

  get baseTitle(): string {
    return this.baseTitle$ || '';
  }

  set baseTitle(title: string) {
    this.baseTitle$ = title;
  }
}
