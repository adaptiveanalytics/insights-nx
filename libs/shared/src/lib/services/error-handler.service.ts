import { ErrorHandler, Injectable } from '@angular/core';
import { ErrorMessage } from '@insights-xplor/data-models';

@Injectable({ providedIn: 'root' })
export class ErrorHandlerService implements ErrorHandler {

  constructor() {
    // do nothing
  }

  // do error handling logic here
  public handleError(err: ErrorMessage, label?: string): void {
    console.error(label);
    console.error(err);
    console.error(err.message);
    console.error(err.stack);
  }

}
