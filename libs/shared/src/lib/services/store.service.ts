import { Inject, Injectable } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import * as semver from 'semver';
import { v4 } from 'uuid';

import { LoggingService } from './logging.service';
import { ModuleConfig } from '@insights-xplor/data-models';

type storeTypes = 'localStorage' | 'sessionStorage';

@Injectable({ providedIn: 'root' })
export class StoreService {

  private env$: ModuleConfig;
  private storeMethod$: storeTypes = 'localStorage';

  constructor(
    @Inject('config') private config: ModuleConfig,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private loggingService: LoggingService,
  ) {
    this.env$ = config;
    const uuid = this.localRetrieve('uuid') as string;

    if (!uuid) {
      this.localSave('uuid', v4());
    }

    const appVersion = this.localRetrieve('appVersion') as string;
    const match = semver.satisfies((appVersion || '0.0.0'), this.env$.appVersion);

    if (!appVersion || !match) {
      this.log('App version changed', 'storeService', 'info');
      this.localSave('appVersion', this.env$.appVersion);
      this.clear();
    } else {
      this.log('no version change detected', 'storeService', 'debug');
    }

    this.log(this, null, 'debug');
  }

  clear(): void {
    const appVersion = this.localRetrieve('appVersion') as string;
    const uuid = this.localRetrieve('uuid') as string;

    this[this.storeMethod].clear();

    this.localSave('appVersion', appVersion);
    this.localSave('uuid', uuid);
  }

  localRetrieve(key: string): unknown {
    return this.localStorage.retrieve(key);
  }

  localSave(key: string, value: unknown): void {
    this.localStorage.store(key, value);
  }

  remove(key: string): void {
    this[this.storeMethod].clear(key);
  }

  retrieve(key: string): unknown {
    return this[this.storeMethod].retrieve(key);
  }

  save(key: string, value: unknown): void {
    this[this.storeMethod].store(key, value);
  }

  sessionRetrieve(key: string): unknown {
    return this.sessionStorage.retrieve(key);
  }

  sessionSave(key: string, value: unknown): void {
    this.sessionStorage.store(key, value);
  }

  get storeMethod() {
    return this.storeMethod$;
  }

  set storeMethod(method: storeTypes) {
    this.storeMethod$ = method;
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
