import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as _ from 'lodash';

import { LoggingService, StoreService } from '../../index';
import { NoticeConfig, NoticeObject } from '@insights-xplor/data-models';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class NoticesService {

  private errorConfig$: NoticeConfig = {
    panelClass: ['notice-error'],
    horizontalPosition: 'center',
    verticalPosition: 'top',
    sound: 'error',
    noticeType: 'error',
  };
  private noticeConfig$: NoticeConfig = {
    duration: 2500,
    panelClass: ['notice-message'],
    horizontalPosition: 'right',
    verticalPosition: 'top',
    sound: 'chat-default',
    noticeType: 'notice',
  };
  private successConfig$: NoticeConfig = {
    duration: 3000,
    panelClass: ['notice-success'],
    horizontalPosition: 'center',
    verticalPosition: 'top',
    sound: 'warning',
    noticeType: 'success',
  };
  private warningConfig$: NoticeConfig = {
    duration: 5000,
    panelClass: ['notice-warning'],
    horizontalPosition: 'center',
    verticalPosition: 'top',
    sound: 'warning',
    noticeType: 'warning',
  };

  private storeName = 'insight-notices';
  private notices: NoticeObject[] = (this.storeService.retrieve(this.storeName) as unknown as NoticeObject[] || []);
  private noticeSub$: BehaviorSubject<NoticeObject[]> = new BehaviorSubject(this.notices);
  noticeState$: Observable<NoticeObject[]> = this.noticeSub$.asObservable();

  constructor(
    private loggingService: LoggingService,
    private matSnackBar: MatSnackBar,
    private storeService: StoreService,
  ) {
    this.log(this, null, 'info');
  }

  clearStore(): void {
    this.storeService.remove(this.storeName);
    this.storeService.save(this.storeName, []);
    this.notices = [];
    this.noticeSub$.next(this.notices);
  }

  getAllNotices(): NoticeObject[] {
    return this.notices;
  }

  getCount(): void {
    this.log(this.notices, 'notices', 'info');
    this.notices = (this.storeService.retrieve(this.storeName) as [] || []);
    this.noticeSub$.next(this.notices);
  }

  pushToStore(notice: NoticeObject): void {
    notice = _.omit(notice, ['action']);
    this.notices.push(notice);
    this.storeService.save(this.storeName, this.notices);
    this.noticeSub$.next(this.notices);
  }

  showError(notice: NoticeObject): void {
    this.showNotice(notice, this.errorConfig$);
  }

  showMessage(notice: NoticeObject): void {
    this.showNotice(notice, this.noticeConfig$);
  }

  showNotice(notice: NoticeObject, config = this.noticeConfig$): void {
    notice.noticeType = config.noticeType;
    config.duration = notice.action ? undefined : config.duration;

    this.pushToStore(notice);

    if (this.notices.length >= 3) {
      this.log('show multi notice message', 'showNotice', 'info');
      // return;
    }

    const sb = this.matSnackBar.open(notice.message, notice.action, config);

    sb.onAction().subscribe(() => {
      this.log('action called', ['showNotice', notice.message], 'info');
      this.clearStore();
    });

    sb.afterDismissed().subscribe(() => {
      this.log('dismissed', ['showNotice', notice.message], 'info');
    });
  }

  showSuccess(notice: NoticeObject): void {
    this.showNotice(notice, this.successConfig$);
  }

  showWarning(notice: NoticeObject): void {
    this.showNotice(notice, this.warningConfig$);
  }

  testNotice(s?: string): void {
    const message: NoticeObject = { message: s || 'message', action: 'X' };

    switch (s) {
      case 'success':
        this.showSuccess(message);
        return;
      case 'warning':
        this.showWarning(message);
        return;
      case 'error':
        this.showError(message);
        return;
      default:
        this.showMessage(message);
    }
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
