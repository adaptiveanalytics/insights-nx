import { Inject, Injectable } from '@angular/core';
import * as _ from 'lodash';
import { datadogLogs as datadog } from '@datadog/browser-logs';
import { ErrorHandlerService } from './error-handler.service';
import { ModuleConfig } from '@insights-xplor/data-models';

export type Log_Levels = 'debug' | 'info' | 'warn' | 'error' | string;

@Injectable({ providedIn: 'root' })
export class LoggingService {

  private readonly env$: ModuleConfig;
  private readonly initialized$?: boolean;
  private readonly logLevel$: Log_Levels = 'debug';
  private readonly logLevels: Log_Levels[] = ['debug', 'info', 'warn', 'error'];

  constructor(
    @Inject('config') private config: ModuleConfig,
    private errorHandler: ErrorHandlerService,
  ) {
    this.env$ = config;
    this.logLevel$ = config.production ? 'debug' : 'info';
    this.initialized$ = config.production;

    if (!config.production) {
      return;
    }

    datadog.init({
      clientToken: config.datadog.clientToken as string,
      site: config.datadog.site,
      env: config.datadog.env,
      version: config.appVersion,
      forwardErrorsToLogs: true,
      sampleRate: 100,
      silentMultipleInit: true,
      useCrossSiteSessionCookie: true,
    });
  }

  public debug(message: string, context?: { [x: string]: unknown }): void {
    if (this.initialized$) {
      datadog.logger.debug(message, context);
    }
  }

  public info(message: string, context?: { [x: string]: unknown }): void {
    if (this.initialized$) {
      datadog.logger.log(message, context, 'info');
      datadog.logger.info(message, context);
    }
  }

  public warn(message: string, context?: { [x: string]: unknown }): void {
    if (this.initialized$) {
      datadog.logger.warn(message, context);
    }
  }

  public error(message: string, context?: { [x: string]: unknown }): void {
    if (this.initialized$) {
      datadog.logger.error(message, context);
    }
  }

  public log(message: string | object | number | unknown, label: string | string[] | null | undefined, level: Log_Levels = this.logLevel$, baseLabel?: string): void {
    if (baseLabel && !label?.includes(baseLabel)) {
      label = _.concat([baseLabel], _.castArray(label) as string[]);
    }

    const labelArr = _.castArray(label).join(':') as string;

    if (!this.logLevels.includes(level)) {
      level = this.logLevel$;
    }

    if (level === 'debug') {
      if (!this.env$.production) {
        console[level](labelArr, message);
      }
    } else {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      console[level](labelArr, message);
    }

    switch (level) {
      case 'debug':
        this.debug(message as string);
        break;
      case 'info':
        this.info(message as string);
        break;
      case 'warn':
        this.warn(message as string);
        break;
      case 'error':
        this.errorHandler.handleError(message as Error, labelArr);
        this.error(message as string);
    }
  }

}
