import { Inject, Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ModuleConfig } from '@insights-xplor/data-models';

@Injectable()
export class EnsureHttpsInterceptor implements HttpInterceptor {
  constructor(@Inject('config') private config: ModuleConfig) {
  }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (!this.config.production) {
      return next.handle(req);
    }

    const secureReq = req.clone({
      url: req.url.replace('http://', 'https://'),
    });

    return next.handle(secureReq);
  }
}
