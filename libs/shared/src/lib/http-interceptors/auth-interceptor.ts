import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { v4 } from 'uuid';

import { StoreService } from '../../index';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private store: StoreService,
  ) {
  }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const url = req.url.replace('https://', '').split('/')[0];

    if (!url.includes('intelli-hub') && !url.includes('localhost')) {
      return next.handle(req);
    }

    const uuid = this.store.retrieve('uuid');
    const setHeaders: { [key: string]: string } = {};
    const reqOptions: { setHeaders: { [key: string]: string } } = {
      setHeaders,
    };

    if (uuid) {
      if (typeof uuid === 'string') {
        setHeaders['uuid'] = uuid;
      }
    } else {
      const reqXuid = req.headers.get('uuid') || v4();
      this.store.save('uuid', reqXuid);
      setHeaders['uuid'] = reqXuid;
    }

    if (Object.keys(setHeaders).length > 0) {
      reqOptions.setHeaders = setHeaders;
    }

    const authReq = req.clone(reqOptions);

    return next.handle(authReq);
  }

}
