import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { LoggingService } from '../../../index';

@Component({
  selector: 'xplor-insights-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.less'],
})
export class NavComponent implements OnInit {
  @Output() clearUserEmit = new EventEmitter<unknown>();

  constructor(
    private loggingService: LoggingService,
  ) {
  }

  ngOnInit(): void {
    this.log(this, null, 'debug');
  }

  clearUser(ev: unknown): void {
    // this.userService.clearUser();
    this.clearUserEmit.emit(ev)
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
