import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticeViewerComponent } from './notice-viewer.component';

describe('NoticeViewerComponent', () => {
  let component: NoticeViewerComponent;
  let fixture: ComponentFixture<NoticeViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoticeViewerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
