import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoggingService, NoticesService } from '../../../index';
import { NoticeObject } from '@insights-xplor/data-models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'xplor-insights-notice-viewer',
  templateUrl: './notice-viewer.component.html',
  styleUrls: ['./notice-viewer.component.less'],
})
export class NoticeViewerComponent implements OnInit, OnDestroy {
  notices: NoticeObject[] = [];
  private noticeState$?: Subscription;

  constructor(
    private loggingService: LoggingService,
    private noticesService: NoticesService,
  ) {
  }

  ngOnInit(): void {
    this.noticeState$ = this.noticesService.noticeState$.subscribe((notices) => this.notices = notices);

    this.log(this, null, 'debug');
  }

  ngOnDestroy() {
    this.noticeState$?.unsubscribe();
  }

  getNotices(): void {
    this.notices = this.noticesService.getAllNotices();
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
