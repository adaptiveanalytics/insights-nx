import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'xplor-insights-loading-screen',
  templateUrl: './loading-screen.component.html',
  styleUrls: ['./loading-screen.component.less'],
})
export class LoadingScreenComponent implements OnInit {

  ngOnInit(): void {
    // eslint-disable-next-line no-restricted-syntax
    console.info(this);
  }
}
