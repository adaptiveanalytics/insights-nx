import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'xplor-insights-loading-panel',
  templateUrl: './loading-panel.component.html',
  styleUrls: ['./loading-panel.component.less']
})
export class LoadingPanelComponent implements OnInit {

  ngOnInit(): void {
    // eslint-disable-next-line no-restricted-syntax
    console.info(this);
  }

}
