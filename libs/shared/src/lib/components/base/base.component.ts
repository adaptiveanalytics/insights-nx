import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { LoggingService, NoticesService } from '../../../index';

@Component({
  selector: 'xplor-insights-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.less'],
})
export class BaseComponent implements OnInit {
  @Output() clearUserEmit = new EventEmitter<unknown>();
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay(),
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private loggingService: LoggingService,
    private noticesService: NoticesService,
  ) {
  }

  ngOnInit(): void {
    this.noticesService.testNotice('message');
    this.log(this, null, 'debug');
  }

  clearUser(ev: unknown): void {
    this.clearUserEmit.emit(ev);
  }

  getCount(): void {
    this.noticesService.getCount();
  }

  testNotice(s?: string): void {
    this.noticesService.testNotice(s);
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
