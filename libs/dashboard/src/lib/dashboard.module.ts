import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '@insights-xplor/shared';
import { WidgetsModule } from '@insights-xplor/widgets';
import { ModuleConfig } from '@insights-xplor/data-models';
import { DashboardBaseComponent } from './dashboard-base/dashboard-base.component';
import { DashboardService } from './dashboard.service';
import { DashboardTemplateComponent } from './dashboard-template/dashboard-template.component';
import { DashboardHeaderComponent } from './dashboard-header/dashboard-header.component';
import { DashboardTileComponent } from './dashboard-tile/dashboard-tile.component';

export const dashboardRoutes: Routes = [
  {
    path: '', // inherited from app-routing.module.ts
    component: DashboardBaseComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: '/dashboard/landing' },
      {
        path: ':id',
        component: DashboardTemplateComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(dashboardRoutes),
    WidgetsModule,
  ],
  declarations: [
    DashboardBaseComponent,
    DashboardTemplateComponent,
    DashboardHeaderComponent,
    DashboardTileComponent,
  ],
  providers: [DashboardService],
})
export class DashboardModule {
  static forRoot(config: ModuleConfig): ModuleWithProviders<DashboardModule> {
    return {
      ngModule: DashboardModule,
      providers: [{ provide: 'config', useValue: config }],
    };
  }
}
