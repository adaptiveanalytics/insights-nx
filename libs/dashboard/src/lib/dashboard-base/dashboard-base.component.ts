import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { DashboardService } from '../dashboard.service';
import { LoggingService, PageService } from '@insights-xplor/shared';

@Component({
  selector: 'insights-xplor-dashboard-base',
  templateUrl: './dashboard-base.component.html',
  styleUrls: ['./dashboard-base.component.less'],
})
export class DashboardBaseComponent implements OnInit, OnDestroy {
  showError = false;
  errMsg?: string;

  private routerSub?: Subscription;

  constructor(
    private dashboardService: DashboardService,
    private loggingService: LoggingService,
    private pageService: PageService,
  ) {
  }

  ngOnInit(): void {
    this.pageService.setTitle('dashboards');

    this.log(this, null, 'debug');
  }

  ngOnDestroy(): void {
    this.routerSub?.unsubscribe();
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
