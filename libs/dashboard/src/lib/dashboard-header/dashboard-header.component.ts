import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { LoggingService } from '@insights-xplor/shared';
import { Dashboard } from '@insights-xplor/data-models';

@Component({
  selector: 'insights-xplor-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.less'],
})
export class DashboardHeaderComponent implements OnInit {
  @Input() dashboard?: Dashboard;
  @Output() editEvent: EventEmitter<Dashboard['id']> = new EventEmitter<Dashboard['id']>();
  @Output() shareEvent: EventEmitter<Dashboard['id']> = new EventEmitter<Dashboard['id']>();

  constructor(
    private loggingService: LoggingService,
  ) {
  }

  ngOnInit(): void {
    this.log(this, null, 'debug');
  }

  editDashboard(): void {
    this.log(this.dashboard?.id, 'editDashboard', 'info');
  }

  shareDashboard(): void {
    this.log(this.dashboard?.id, 'shareDashboard', 'info');
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
