import { Inject, Injectable } from '@angular/core';
import *  as _ from 'lodash';
import { AngularFirestore } from '@angular/fire/compat/firestore';

import { LoggingService } from '@insights-xplor/shared';
import { Dashboard, ModuleConfig } from '@insights-xplor/data-models';
import { WidgetService } from '@insights-xplor/widgets';
import dashboardJson from '../assets/dashboards.json';

@Injectable({ providedIn: 'root' })
export class DashboardService {
  private env$: ModuleConfig;
  private dashboards: Dashboard[] = dashboardJson;

  constructor(
    @Inject('config') private config: ModuleConfig,
    private afs: AngularFirestore,
    private widgetService: WidgetService,
    private loggingService: LoggingService,
  ) {
    this.env$ = this.config;

    this.log(this, null, 'info');
  }

  async getDashboardById(id: Dashboard['id']): Promise<Dashboard | void> {
    const dashboard = _.find(this.dashboards, (d) => d.id === String(id));

    if (dashboard) {
      return dashboard as Dashboard;
    }

    return;
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
