import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { LoggingService } from '@insights-xplor/shared';
import { DashboardTile } from '@insights-xplor/data-models';

@Component({
  selector: 'insights-xplor-dashboard-tile',
  templateUrl: './dashboard-tile.component.html',
  styleUrls: ['./dashboard-tile.component.less'],
})
export class DashboardTileComponent implements OnInit {
  @Input() tile?: DashboardTile;
  @Output() exportEvent: EventEmitter<DashboardTile['id']> = new EventEmitter();
  @Output() refreshEvent: EventEmitter<DashboardTile['id']> = new EventEmitter();

  constructor(
    private loggingService: LoggingService,
  ) {
  }

  ngOnInit(): void {
    this.log(this, null, 'debug');
  }

  export(): void {
    this.exportEvent.emit(this.tile?.id);
  }

  refresh(): void {
    this.refreshEvent.emit(this.tile?.id);
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
