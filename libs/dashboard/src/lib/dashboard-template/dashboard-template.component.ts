import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { DashboardService } from '../dashboard.service';
import { LoggingService, PageService } from '@insights-xplor/shared';
import { Dashboard, DashboardTile, ErrorMessage } from '@insights-xplor/data-models';

@Component({
  selector: 'insights-xplor-dashboard-template',
  templateUrl: './dashboard-template.component.html',
  styleUrls: ['./dashboard-template.component.less'],
})
export class DashboardTemplateComponent implements OnInit, OnDestroy {
  dashboard?: Dashboard;
  dashboardId?: Dashboard['id'];
  errMsg?: ErrorMessage;

  private routeSub?: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private dashboardService: DashboardService,
    private loggingService: LoggingService,
    private pageService: PageService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.paramMap.subscribe(async (pm) => {
      const id = pm.get('id');

      if (id) {
        try {
          await this.loadDashboard(id);
        } catch (e) {
          this.log(e, 'routeSub', 'error');
        }
      }
    });

    this.log(this, null, 'info');
  }

  ngOnDestroy(): void {
    this.routeSub?.unsubscribe();
  }

  async editDashboard(id: Dashboard['id']): Promise<void> {
    this.log(id, 'editDashboard', 'info');
  }

  async exportTile(id: DashboardTile['id']): Promise<void> {
    this.log(id, 'exportTile', 'info');
  }

  /**
   * Load dashboard by ID
   * @param {string} id
   */
  async loadDashboard(id: Dashboard['id']): Promise<void> {
    this.dashboard = undefined;
    this.errMsg = undefined;

    if (id === 'landing') {
      this.errMsg = { name: 'Dashboard Error', message: 'No dashboard ID provided.' };
      this.pageService.setTitle('dashboard home');
      return;
    }

    try {
      const dashboard = await this.dashboardService.getDashboardById(id);

      if (dashboard) {
        this.dashboardId = id;
        this.dashboard = dashboard;
        this.pageService.setTitle(dashboard.title);
        return;
      }

    } catch (e) {
      this.router.navigate(['/dashboard']).catch(e => this.log(e, 'loadDashboard', 'error'));
    }
  }

  async refreshTile(id: DashboardTile['id']): Promise<void> {
    this.log(id, 'refreshTile', 'info');
  }

  async shareDashboard(id: Dashboard['id']): Promise<void> {
    this.log(id, 'shareDashboard', 'info');
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
