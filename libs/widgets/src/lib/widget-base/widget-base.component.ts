import { Component, Input, OnInit } from '@angular/core';
import { LoggingService } from '@insights-xplor/shared';
import { WidgetService } from '../widget.service';
import { Widget } from '@insights-xplor/data-models';

@Component({
  selector: 'insights-xplor-widget-base',
  templateUrl: './widget-base.component.html',
  styleUrls: ['./widget-base.component.less'],
})
export class WidgetBaseComponent implements OnInit {
  @Input() widgetId?: Widget['id'];

  errMsg?: { message: string };
  widget?: Widget;

  constructor(
    private widgetService: WidgetService,
    private loggingService: LoggingService,
  ) {
  }

  ngOnInit(): void {
    if (!this.widgetId) {
      this.errMsg = { message: 'no widget id' };
    } else {
      this.loadWidget(this.widgetId);
    }

    this.log(this, null, 'debug');
  }

  loadWidget(id: Widget['id']): void {
    this.errMsg = undefined;

    const widget = this.widgetService.getWidgetById(id);

    if (widget) {
      this.widget = widget;
      return;
    }

    this.errMsg = { message: 'widget not found ' };
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
