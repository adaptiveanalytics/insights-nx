import { Inject, Injectable } from '@angular/core';
import * as _ from 'lodash';
import { AngularFirestore } from '@angular/fire/compat/firestore';

import { ModuleConfig } from '@insights-xplor/data-models';
import { LoggingService } from '@insights-xplor/shared';
import { Widget } from '@insights-xplor/data-models';
import demoWidgets from '../assets/widgets.json';

@Injectable({ providedIn: 'root' })
export class WidgetService {
  private widgets: Widget[] = demoWidgets as Widget[];

  constructor(
    @Inject('config') private config: ModuleConfig,
    private afs: AngularFirestore,
    private loggingService: LoggingService,
  ) {
    this.log(this, null, 'debug');
  }

  getWidgetById(id: string): Widget {
    const widget = _.find(this.widgets, { id });

    return widget as Widget;
  }

  log(msg: unknown, label?: string | string[] | undefined | null, level?: string): void {
    this.loggingService.log(msg, label, level, this.constructor.name);
  }
}
