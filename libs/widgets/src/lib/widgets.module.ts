import { ModuleWithProviders, NgModule } from '@angular/core';

import { SharedModule } from '@insights-xplor/shared';
import { WidgetService } from './widget.service';
import { WidgetBaseComponent } from './widget-base/widget-base.component';
import { ModuleConfig } from '@insights-xplor/data-models';

const COMPONENTS: unknown[] = [
  WidgetBaseComponent,
];

@NgModule({
  declarations: [
    COMPONENTS,
  ],
  imports: [
    SharedModule,
  ],
  exports: [
    [COMPONENTS],
  ],
  providers: [
    WidgetService,
  ],
})
export class WidgetsModule {
  forRoot(config: ModuleConfig): ModuleWithProviders<WidgetsModule> {
    return {
      ngModule: WidgetsModule,
      providers: [{ provide: 'config', useValue: config }],
    };
  }
}
